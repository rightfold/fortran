! Fretboard calculator.
program fretcalc
implicit none

real :: fconst = 2.0 ** (1.0 / 12.0)
real :: scale_length = 25.5     ! [inches]
integer :: total_frets = 24
integer :: fret
integer :: u

100 format (i3, 5x, f5.2)
open(newunit=u, file='frets.dat')
do fret = 1, total_frets
    write (unit=u, fmt=100) fret, scale_length / (fconst ** fret)
end do
close(unit=u)

end program fretcalc
