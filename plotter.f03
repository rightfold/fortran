program plotter
use gnuplot_fortran
implicit none

integer, parameter :: n = 100
real, dimension(0:n) :: x, y
real :: x_start = 0.0, x_end = 20.0, dx
integer :: i

dx = (x_end - x_start) / real(n)
x(0:n) = [(i * dx, i=0, n)]

y = sin(x) / (x + 1.0)

call plot2d(x, y)

end program
