module gnuplot_fortran
implicit none

contains

subroutine plot2d(x, y)
    real, intent(in), dimension(:) :: x, y
    integer :: u, i

    if (size(x) /= size(y)) then
        print *, "array size mismatch"
        stop
    end if

    open(newunit=u, file='data.dat')
    do i = 1, size(x)
        write(unit=u, fmt=*), x(i), ' ', y(i)
    end do
    close(unit=u)

    call system('gnuplot -p -e "plot ''data.dat'' using 1:2"')
end subroutine plot2d

end module gnuplot_fortran
